window.addEventListener("DOMContentLoaded", () => {
    const Modal = {
        body: document.querySelector("body"),
        modal: document.querySelector(".modal"),
        btnOpen: document.querySelector("#openModal"),
        btnClose: document.querySelector("#closeModal"),
        btnSend: document.querySelector("#sendModal"),
        userName: document.querySelector("#name"),
        userEmail: document.querySelector("#Email"),
        userMessage: document.querySelector("#message"),
        modalSucces: document.querySelector(".modalSucces"),
        
        init: function() {
            this.modal.addEventListener("click", (e) => {
                if (e.target.classList.contains("modal")) {
                    this.closeModal();
                }
            });
            this.btnOpen.addEventListener("click", this.openModal.bind(Modal));
            this.btnClose.addEventListener("click", this.closeModal.bind(Modal));
            this.btnSend.addEventListener("click", this.validate.bind(Modal));
        },
        openModal: function() {
            this.modal.classList.add("modal_js");
            this.body.classList.add("body_js");
        },
        closeModal: function() {
            this.modal.classList.remove("modal_js");
            this.body.classList.remove("body_js");
        },
        openModalSucces: function() {
            this.modal.classList.remove("modal_js");
            this.modalSucces.classList.add("modalSucces_js");
        },
        validate: function() {
            if (this.userName.value != "" && this.userEmail.value != "" && this.userMessage.value != "") {
                this.openModalSucces();
            }
        }
    }

    const ModalSucces = {
        body: document.querySelector("body"),
        modal: document.querySelector(".modalSucces"),
        btnClose: document.querySelector("#closeModalSucces"),

        init: function() {
            this.modal.addEventListener("click", (e) => {
                if (e.target.classList.contains("modalSucces_js")) {
                    this.closeModal();
                }
            });
            this.btnClose.addEventListener("click", this.closeModal.bind(ModalSucces));
        },
        closeModal: function() {
            this.modal.classList.remove("modalSucces_js");
            this.body.classList.remove("body_js");
        },
    }

    Modal.init();
    ModalSucces.init();
})